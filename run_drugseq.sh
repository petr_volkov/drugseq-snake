#!/usr/bin/env bash

snakedir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
workdir=$1

echo "snakemake -s ${snakedir}/Snakefile -d ${workdir} --cores 8 --use-singularity --use-conda" #-u ${snakedir}/Configs/cluster.yaml --profile ${snakedir}/Snakemake_Profile"
