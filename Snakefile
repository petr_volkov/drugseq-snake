import os

SNAKEDIR = os.path.dirname(workflow.snakefile)

rule all:
    input: kallisto = "intermediate/kallisto.index",
           output = "output/quant/output.bus"

rule download_gtf:
    output: "reference/transcrips.gtf"
    shell: "wget -O - ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.annotation.gtf.gz | zcat > {output}"

rule download_fasta:
    output: fasta = "reference/transcripts.fasta"
    shell: "wget -O - ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.transcripts.fa.gz | zcat > {output}"

rule kallisto_index:
    input: fasta = "reference/transcripts.fasta",
    output: "intermediate/kallisto.index"
#    conda: "conda/kallisto.yaml"
    singularity: "docker://zavolab/kallisto:0.46.1"           
    shell: "kallisto index --index {output} {input.fasta}"
    
rule kallisto_bus:
    input: file1 = "data/1_S1_R1_001.fastq.gz",
           file2 = "data/1_S1_R2_001.fastq.gz",
           index = "intermediate/kallisto.index"
    output: "output/quant/output.bus"
    params: outdir = "output/quant"
    #conda: "conda/kallisto.yaml"
    singularity: "docker://zavolab/kallisto:0.46.1"
    shell: "kallisto bus --index {input.index} --output {params.outdir} -x 0,0,10:0,10,20:1,0,0 {input.file1} {input.file2}"

# rule produce_tx2gene:
#     input: "reference/transcrips.gtf"
#     output: rdata = "intermediate/tx2gene.RData",
#             tsv = "reference/tx2gene.tsv"
#     params: tx2gene_script = f"{SNAKEDIR}/code/r/make_tx2gene.R"
#     conda: "conda/r.yaml"
#     shell: "Rscript {params.tx2gene_script} {input} {output.rdata} {output.tsv}"
